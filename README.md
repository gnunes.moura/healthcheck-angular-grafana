# Embedded Grafana Dashboard on Angular ~12.2.0
Prove of Concept of a simple status report dashboard.

## Why?
We need to create a view for the users know about the health of applications and
dependencies, but we don`t want to create a full web applicaton with logic to
show this.

## What?
In order to keep the least effort and delivery a simple solution we will use the
Grafana to create and keep the panels with information and the web app 
presenting then to the user.

## How?
We need to configure the Grafana, any proxy between the grafana and client, and 
the component.

### Component configuration
The component needs to use the dom sanitizer to set the url as a trustfull 
resource and use it on the component HTML definition.
```ts
constructor(private sanitizer: DomSanitizer) {
  this.iFrameUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
}
```

```html
<iframe [src]="iFrameUrl"></iframe>
```

## Proxy configuration
The proxy must be configure `X-Frame-Options` to allow from the client origin.

## Grafana Configuration
The Grafana must be configured to allow embedding of content, see the 
[documentation](https://grafana.com/docs/grafana/latest/administration/configuration/#allow_embedding)
